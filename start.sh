#!/bin/bash

set -eux

mkdir -p /run/wallabag/cache /run/wallabag/logs /run/wallabag/sessions

sed -e "s/database_driver:.*/database_driver: pdo_pgsql/" \
    -e "s/database_host:.*/database_host: ${CLOUDRON_POSTGRESQL_HOST}/" \
    -e "s/database_port:.*/database_port: ${CLOUDRON_POSTGRESQL_PORT}/" \
    -e "s/database_name:.*/database_name: ${CLOUDRON_POSTGRESQL_DATABASE}/" \
    -e "s/database_user:.*/database_user: ${CLOUDRON_POSTGRESQL_USERNAME}/" \
    -e "s/database_password:.*/database_password: ${CLOUDRON_POSTGRESQL_PASSWORD}/" \
    -e "s/database_charset: .*/database_charset: utf8/" \
    -e "s,domain_name: .*,domain_name: ${CLOUDRON_APP_ORIGIN}," \
    -e "s/redis_host:.*/redis_host: ${CLOUDRON_REDIS_HOST}/" \
    -e "s/redis_port:.*/redis_port: ${CLOUDRON_REDIS_PORT}/" \
    -e "s/redis_password:.*/redis_password: ${CLOUDRON_REDIS_PASSWORD}/" \
    -e "s/mailer_host:.*/mailer_host: ${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/mailer_user:.*/mailer_user: ${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/mailer_password:.*/mailer_password: ${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/from_email:.*/from_email: ${CLOUDRON_MAIL_FROM}/" \
    -e "s/twofactor_sender:.*/twofactor_sender: ${CLOUDRON_MAIL_FROM}/" \
    -e "s/fosuser_registration:.*/fosuser_registration: false/" \
    -e "s/fosuser_confirmation:.*/fosuser_confirmation: false/" \
    /app/code/parameters.yml > /run/wallabag/parameters.yml

sed -e "s/save_path:.*/save_path: '\/run\/wallabag\/sessions'/" \
    /app/code/config.yml > /run/wallabag/config.yml

sed -e "s/path:.*/path: '\/run\/wallabag\/wallabag.log'/" \
    /app/code/config_prod.yml > /run/wallabag/config_prod.yml

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# ensure the upload, import and images directory exists
mkdir -p /app/data/uploads/import /app/data/assets/images /app/data/data

chown -R www-data:www-data /run/wallabag /app/data

gosu="/usr/local/bin/gosu www-data:www-data"

# Install wallabag, if this is an update, the script will warn about existing user "wallabag". This is ok.
$gosu php bin/console wallabag:install --env prod --no-interaction

# run the migration scripts
$gosu php bin/console doctrine:migrations:migrate --env prod --no-interaction

# update the location
$gosu php bin/console doctrine:query:sql --env prod "UPDATE wallabag_internal_setting SET value='1' WHERE name='import_with_redis';"

# generate URL hashes
$gosu php bin/console wallabag:generate-hashed-urls --env=prod

# start the import worker
$gosu php bin/console wallabag:import:redis-worker --env prod pocket -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod readability -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod instapaper -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod wallabag_v1 -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod wallabag_v2 -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod firefox -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod chrome -vv &

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
