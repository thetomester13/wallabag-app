# Wallabag Cloudron App

This repository contains the Cloudron app package source for [Wallabag](https://wallabag.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.wallabag.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.wallabag.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd wallabag-app

cloudron build
cloudron install
```
