#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var TEST_TIMEOUT = 30000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var USERNAME = 'wallabag';
    var PASSWORD = 'wallabag';
    var TEST_URL = 'https://cloudron.io';

    var app;
    var browser;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(callback) {
        browser.manage().deleteAllCookies();

        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.id('username'));
        }).then(function () {
            return browser.findElement(By.id('username')).sendKeys(USERNAME);
        }).then(function () {
            return browser.findElement(By.id('password')).sendKeys(PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//form[@name="loginform"]')).submit();
        }).then(function () {
            return waitForElement(By.id('news_menu'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.id('news_menu'));
        }).then(function () {
            return browser.findElement(By.id('news_menu')).click();
        }).then(function () {
            return waitForElement(By.xpath('//a[@href="/logout"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[@href="/logout"]')).click();
        }).then(function () {
            return waitForElement(By.id('username'));
        }).then(function () {
            callback();
        });
    }

    function loginOld(callback) {
        browser.manage().deleteAllCookies();

        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.id('username'));
        }).then(function () {
            return browser.findElement(By.id('username')).sendKeys(USERNAME);
        }).then(function () {
            return browser.findElement(By.id('password')).sendKeys(PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//form[@name="loginform"]')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//a[@href="/logout"]'));
        }).then(function () {
            callback();
        });
    }

    function logoutOld(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.xpath('//a[@href="/logout"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[@href="/logout"]')).click();
        }).then(function () {
            return waitForElement(By.id('username'));
        }).then(function () {
            callback();
        });
    }

    function urlExists(callback) {
        browser.get(`https://${app.fqdn}/view/1`).then(function () {
            return waitForElement(By.xpath('//h2[text()="Instantly install Apps"]'));
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function addLink(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.xpath('//a[@href="/new"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[@href="/new"]')).click();
        }).then(function () {
            return waitForElement(By.id('entry_url'));
        }).then(function () {
            return browser.findElement(By.id('entry_url')).sendKeys(TEST_URL);
        }).then(function () {
            return browser.findElement(By.id('entry_url')).sendKeys(Key.ENTER);
        }).then(function () {
            return waitForElement(By.xpath('//a[@href="/view/1"]'));
        }).then(function () {
            callback();
        });
    }

    function registrationDisabled(done) {
        superagent.get('https://' + app.fqdn + '/register').redirects(0).end(function (error, result) {
            if (result.statusCode !== 301) return done(new Error('register is not redirecting ' + result.statusCode));

            done();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.wallabag.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', loginOld);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logoutOld);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can login', login);
    it('url exists', urlExists);
    it('disabled registration', registrationDisabled);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            done();
        });
    });
});
